{-# LANGUAGE TemplateHaskell #-}
module Rx.Logger.Env (Settings(..), defaultSettings, setupTracer) where

import Control.Monad.Trans (lift)
import Data.Monoid         (First (..), mconcat)
import System.Environment  (lookupEnv)
import System.IO           (stderr, stdout)


import           Rx.Disposable (Disposable)
import qualified Rx.Disposable as Disposable

import Rx.Logger.Core       (scope)
import Rx.Logger.Format     (ttccFormat)
import Rx.Logger.Serializer (serializeToFile, serializeToHandle)
import Rx.Logger.Types


--------------------------------------------------------------------------------

data Settings
  = Settings {
    envPrefix            :: String
  , envLogEntryFormatter :: LogEntryFormatter
  }

defaultSettings :: Settings
defaultSettings =
  Settings { envPrefix = "LOGGER"
           , envLogEntryFormatter = ttccFormat }

setupTracer :: (HasLogger logger)
             => Settings
             -> logger
             -> IO Disposable
setupTracer settings logger = do
    allDisposables <- Disposable.newCompositeDisposable
    setupHandleTrace >>= flip Disposable.append allDisposables
    setupFileTrace   >>= flip Disposable.append allDisposables
    return $! Disposable.toDisposable allDisposables
  where
    entryF = envLogEntryFormatter settings
    prefix = envPrefix settings
    setupFileTrace =
      lookupEnv (prefix ++ "_TRACE_FILE")
          >>= maybe Disposable.emptyDisposable
                    (\filepath ->
                        scope logger 'setupTracer $ do
                          disp <- lift $ serializeToFile filepath entryF logger
                          configMsg $ "Log tracing on file " ++ filepath
                          return disp)
    setupHandleTrace = do
      -- NOTE: Can use either STDOUT or STDERR for logging
      results <- sequence [
          lookupEnv (prefix ++ "_TRACE_STDOUT")
            >>= return . maybe Nothing (const $ Just stdout)
        , lookupEnv (prefix ++ "_TRACE_STDERR")
            >>= return . maybe Nothing (const $ Just stderr)
        ]
      case getFirst . mconcat . map First $ results of
        Just handle -> do
          sub <- serializeToHandle handle entryF logger
          scope logger 'setupTracer $ do
            configMsg $ "Log tracing on handle " ++ show handle
            return sub
        Nothing -> Disposable.emptyDisposable

--------------------------------------------------------------------------------
