{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DeriveDataTypeable #-}
{-# LANGUAGE ExistentialQuantification #-}
{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE PackageImports             #-}
module Rx.Logger.Types
       ( module Rx.Logger.Types
       , MonadIO(..)
       , MonadBaseControl
       , Name
       , LText.Text
       , ReaderT
       , ask
       , local
       , runReaderT
       , fromLogMsg
       ) where


import Data.Typeable (Typeable, cast)
import GHC.Generics (Generic)

import           Control.Concurrent          (ThreadId, myThreadId)
import           Control.Monad.Trans         (MonadIO (..))
import           Control.Monad.Trans.Control (MonadBaseControl)
import           Control.Monad.Trans.Reader  (ReaderT, ask, local, runReaderT)

import qualified Data.Text                   as Text
import qualified Data.Text.Lazy              as LText
import           Data.Time                   (UTCTime, getCurrentTime)

import           Language.Haskell.TH.Syntax  (Name)

import           Rx.Observable               (onNext)
import           Rx.Subject                  (Subject)

--------------------------------------------------------------------------------

fromLogMsg :: Typeable a => LogMsg -> Maybe a
fromLogMsg (LogMsg a) = cast a

class (Show a, Typeable a) => ToLogMsg a where
  toLogMsg :: a -> LText.Text
  toLogMsg = LText.pack . show


data LogMsg
  = forall a. ToLogMsg a => LogMsg a
  deriving (Typeable)

instance ToLogMsg LText.Text where
  toLogMsg = id

instance ToLogMsg Text.Text where
  toLogMsg = LText.fromChunks . (:[])


instance ToLogMsg String where
  toLogMsg = LText.pack

instance ToLogMsg LogMsg where
  toLogMsg (LogMsg a) = toLogMsg a

instance Show LogMsg where
  show (LogMsg a) = show a

data LogEntry
  = LogEntry {
    _logEntryScope     :: !Name
  , _logEntryTimestamp :: !UTCTime
  , _logEntryMsg       :: !LogMsg
  , _logEntryLevel     :: !LogLevel
  , _logEntryThreadId  :: !ThreadId
  }
  deriving (Show, Typeable, Generic)

type Logger = Subject IO LogEntry

--------------------------------------------------------------------------------

data LogLevel
  = FINE    -- ^ Indicates normal tracing information.

  | FINER   -- ^ Indicates a fairly detailed tracing message. By
            -- default logging calls for entering, returning, or
            -- throwing an exception are traced at this level
  | FINEST  -- ^ Indicates a highly detailed tracing message

  | CONFIG  -- ^ Intended to provide a variety of static configuration
            -- information, to assist in debugging problems that may be
            -- associated with particular configurations.

  | INFO    -- ^ Describe events to be used for reasonably
            -- significant messages that will make sense to end users
            -- and system administrators

  | WARNING -- ^ Describe events that will be of interest to
            -- end users or system managers, or which indicate
            -- potential problems

  | SEVERE  -- ^ Describe events that are of considerable importance
            -- and which will prevent normal program execution. They
            -- should be reasonably intelligible to end users and to
            -- system administrators

  | NONE    -- ^ Special level that can be used to turn off logging
  deriving (Show, Eq, Ord, Enum, Generic, Typeable)

class MonadLog m where
  logMsg :: ToLogMsg a => LogLevel -> a -> m ()

trace :: (MonadLog m, ToLogMsg a) => a -> m ()
trace = logMsg FINE

finer :: (MonadLog m, ToLogMsg a) => a -> m ()
finer = logMsg FINER

finest :: (MonadLog m, ToLogMsg a) => a -> m ()
finest = logMsg FINEST

configMsg :: (MonadLog m, ToLogMsg a) => a -> m ()
configMsg = logMsg CONFIG

info :: (MonadLog m, ToLogMsg a) => a -> m ()
info = logMsg INFO

warn :: (MonadLog m, ToLogMsg a) => a -> m ()
warn = logMsg WARNING

severe :: (MonadLog m, ToLogMsg a) => a -> m ()
severe = logMsg SEVERE

--------------------

instance (MonadBaseControl IO m, MonadIO m)
         => MonadLog (ReaderT (Name, Logger) m) where
  logMsg level msg = do
    (name, tracer) <- ask
    time <- liftIO getCurrentTime
    tid  <- liftIO myThreadId
    liftIO $ onNext tracer $! LogEntry name time (LogMsg msg) level tid

instance (MonadBaseControl IO m, MonadIO m)
         => MonadLog (ReaderT (LogLevel, Name, Logger) m) where
  logMsg _ msg = do
    (level, name, tracer) <- ask
    time <- liftIO getCurrentTime
    tid  <- liftIO myThreadId
    liftIO $ onNext tracer $! LogEntry name time (LogMsg msg) level tid

--------------------------------------------------------------------------------

class HasLogger a where
  getLogger :: a -> Logger

instance HasLogger (Subject IO LogEntry) where
  getLogger = id

--------------------------------------------------------------------------------

type LogEntryFormatter = LogEntry -> LText.Text
