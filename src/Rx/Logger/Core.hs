{-# LANGUAGE FlexibleContexts #-}
module Rx.Logger.Core where

import Data.Text.Format (Format, format)
import Data.Text.Format.Params (Params)

import Rx.Subject (publishSubject)
import Rx.Logger.Types

--------------------------------------------------------------------------------

newLogger :: IO Logger
newLogger = publishSubject

--------------------------------------------------------------------------------

scope :: ( MonadBaseControl IO m, MonadIO m, HasLogger s )
      => s
      -> Name
      -> ReaderT (Name, Logger) m result
      -> m result
scope s name action = runReaderT action (name, getLogger s)

scopeWithLevel
  :: ( MonadBaseControl IO m, MonadIO m, HasLogger s )
  => LogLevel
  -> s
  -> Name
  -> ReaderT (LogLevel, Name, Logger) m result
  -> m result
scopeWithLevel level s name action =
  runReaderT action (level, name, getLogger s)

--------------------------------------------------------------------------------

logFormat :: (MonadLog m, Params p) => LogLevel -> Format -> p -> m ()
logFormat level txtFormat params = do
  logMsg level $ format txtFormat params

traceFormat :: (MonadLog m, Params p) => Format -> p -> m ()
traceFormat = logFormat FINE

finerFormat :: (MonadLog m, Params p) => Format -> p -> m ()
finerFormat = logFormat FINER

finestFormat :: (MonadLog m, Params p) => Format -> p -> m ()
finestFormat = logFormat FINEST

configFormat :: (MonadLog m, Params p) => Format -> p -> m ()
configFormat = logFormat CONFIG

infoFormat :: (MonadLog m, Params p) => Format -> p -> m ()
infoFormat = logFormat INFO

warnFormat :: (MonadLog m, Params p) => Format -> p -> m ()
warnFormat = logFormat WARNING

severeFormat :: (MonadLog m, Params p) => Format -> p -> m ()
severeFormat = logFormat SEVERE

--------------------------------------------------------------------------------
